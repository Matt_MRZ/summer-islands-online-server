/**
 *
 * Description:
 * Summer Islands Online Server
 *
 * @author Matthias Maerz.
 * @since  0.1.1
 */

const fs = require("fs");
const path = require('path');

	let lobbyManager;

	let app = require('express')();
	let server = require('http').createServer(app);
	let port = 21621;

	let io = require('socket.io')(server,
	  {
		transports: ['websocket'],
		forceNew: true,
		pingInterval: 5000,
		pingTimeout: 20000
	  });
	  
	server.listen(port, ()=>{
	  console.log('listening on *:' + port);
	  
	  lobbyManager = new lobbyManagerClass();
	});
	  
	//########### SERVER WEBSITE ##################

	app.get('/', (req, res) => {
		try{
			res.sendFile(path.join(__dirname + '/index.html'));
		} catch (err) {
			writeErr(err); 
		}
	});
	
	app.get('/stats', (req, res) => {
		
		try{
			let lobbies = [];
			
			lobbyManager.lobbyListe.forEach(function (item) {
				lobbies.push({name: ''+item.name, maxPlayer: ''+item.playeranzahl, playeranzahl: ''+item.playerlist.getPlayerAnzahl(), openlobby: ''+item.openlobby});
			});
			
			res.set({ 'Content-Type': 'application/json; charset=utf-8' });
			res.json({lobbies});
		} catch (err) {
			writeErr(err); 
		}
	});


	//########### Client Connections ##################

	io.on('connection', (socket) => {
	  console.log(new Date().toISOString() + ";connection;"+socket.id);
	  
	  socket.on('disconnect', ()=>{
		console.log(new Date().toISOString() + ";disconnect;"+socket.id);
		if(socket.namespace !== undefined){
			leaveLobby(socket.namespace, socket);
		}
	  });
	  
	  socket.on('createLobby', ({name, pass}) => { 
		try{
			createLobby(name, pass, socket);
		} catch (err) {writeErr(err)}
	  });
	  
	  socket.on('newPlayerAnzahl', ({name, anzahl})=>{
		  try{
			newPlayerAnzahl(name, socket, anzahl);
		} catch (err) {writeErr(err)}
	  });
	  
	  socket.on('connectToLobby', ({name, pass})=>{
		try{
			connectToLobby(name, pass, socket);
		} catch (err) {writeErr(err)}
	  });
	  
	  socket.on('getAllLobbys', ()=>{
		try{
			getAllLobbys(socket);
		} catch (err) {writeErr(err)}
	  });
	  
	  socket.on('leaveLobby', ({name})=>{
		try{
			leaveLobby(name, socket);
		} catch (err) {writeErr(err)}
	  });
	  
	  socket.on('goMatch', ({name})=>{ 
		try{
			goMatch(name, socket);
		} catch (err) {writeErr(err)}
	  });
	  
	});

	//###############  Namespace ####################

	function createNamespace(name, socketHost){
		let ioNamespace = io.of('/'+name);
		ioNamespace.on('connection', (socket) => {
			if(lobbyManager.lobbyListe.get(''+name).openlobby){
				connectNamespaceToLobby(name, socket, socket.handshake.query.socketIDLobby);
				console.log(new Date().toISOString() + ";createNamespace_connection;"+ name + ";"+socket.id);
			}else{
				socket.disconnect();
			}

			socket.on('disconnect', ()=>{
				try{
					socketNamespaceDisconnected(name, socket);
					if(socketHost.socketNamespace.id !== socket.id){
						ioNamespace.to(socketHost.socketNamespace.id).emit('toHost',{event: 'playerLeftMatch'});
					}
					console.log(new Date().toISOString() + ";createNamespace_disconnect;"+ name + ";"+socket.id);
				} catch (err) {writeErr(err)}
			});
			
			socket.on('toHost', ({event, msg})=>{
				try{
					ioNamespace.to(socketHost.socketNamespace.id).emit('toHost',{event: event, msg: msg});
				} catch (err) {writeErr(err)}
			});
			
			socket.on('socket', ({event, msg, socketID})=>{
				try{
					ioNamespace.to(socketID).emit(event, msg);
				} catch (err) {writeErr(err)}
			});
			socket.on('emit', ({event, msg})=>{
				try{
					socket.broadcast.emit(event, msg);
				} catch (err) {writeErr(err)}
			});
			
			socket.on('broadcast', ({event, msg, socketID})=>{
				try{
					io.of('/'+name).sockets.forEach(function (item) {
						if(item.id !== socketHost.socketNamespace.id && item.id !== socketID){
							ioNamespace.to(item.id).emit(event, msg);
						}
					});
				} catch (err) {writeErr(err)}
			});
			
			socket.on('gotKicked', ({socketID})=>{
				try{
					if(io.of('/'+name).sockets.has(socketID)){
						socketNamespaceDisconnected(name, io.of('/'+name).sockets.get(socketID));
						io.of('/'+name).sockets.get(socketID).disconnect();
					}
				} catch (err) {writeErr(err)}
			});
		});
		return ioNamespace;
	}

	function deleteNamespace(name){
		let ioNamespace = io.of('/'+name);
		ioNamespace.sockets.forEach(function (item) {
			item.disconnect();
		});
		ioNamespace.removeAllListeners(); 
	}

	//############### Functions ####################

	function createLobby(name, pass, socketHost){
		lobbyManager.createLobby(name, pass, socketHost);
	}

	function connectToLobby(name, pass, socketClient){
		lobbyManager.connectToLobby(name, pass, socketClient);
	}

	function getAllLobbys(socketClient){
		lobbyManager.getAllLobbys(socketClient);
	}

	function leaveLobby(name, socketClient){
		lobbyManager.leaveLobby(name, socketClient);
	}

	function newPlayerAnzahl(name, hostClient, anzahl){
		lobbyManager.newPlayerAnzahl(name, hostClient, anzahl);
	}

	function connectNamespaceToLobby(name, socketNamespace, socketIDLobby){
		lobbyManager.connectNamespaceToLobby(name, socketNamespace, socketIDLobby);
	}

	function socketNamespaceDisconnected(name, socketNamespace){
		lobbyManager.socketNamespaceDisconnected(name, socketNamespace);
	}
	
	function goMatch(name, hostClient){
		lobbyManager.goMatch(name, hostClient);
	}
	
	//#################      CLASSES      #####################
	//################# lobbyManager Class #####################
		
	class lobbyManagerClass {
		constructor(){
			this.lobbyListe = new Map();
		}
		
		createLobby(name, pass, socketHost){
			try{
				if(this.lobbyListe.has(name)){
					io.to(socketHost.id).emit('news', 'Name already in use');
				}else{
					if(isStringAlphanumeric(''+name)){
						this.lobbyListe.set('' + name, new lobbyClass(name, pass, socketHost));
						io.to(socketHost.id).emit('lobbyCreated');
						console.log(new Date().toISOString() + ";newLobby;"+ name + ";"+socketHost.id);
					}
				}
			} catch (err) {writeErr(err)}
		}
		
		connectToLobby(name, pass, socketClient){
			try{
				this.lobbyListe.get(''+name).connectToLobby(pass, socketClient);
			} catch (err) {writeErr(err)}
		}
		
		getAllLobbys(socketClient){
			try{
				let allLobbys = [];
				this.lobbyListe.forEach(function (lobby, key) {
					if(lobby.openlobby){
						allLobbys.push(lobby.getLobbyInfo());
					}
				});
				io.to(socketClient.id).emit('getAllLobbys', allLobbys);
			} catch (err) {writeErr(err)}
		}
		
		leaveLobby(name, socketClient){
			try{
				if(this.lobbyListe.has(''+name)){
					let closeLobby = this.lobbyListe.get(''+name).leaveLobby(socketClient, 0);
					if(closeLobby){
						deleteNamespace(name)
						this.lobbyListe.delete(name);
						console.log(new Date().toISOString() + ";closeLobby;"+name+";"+socketClient.id);
					}
				}
			} catch (err) {writeErr(err)}
		}
		
		closeLobby(name){
			try{
				if(this.lobbyListe.has(''+name)){
					this.lobbyListe.delete(name);
				}
			} catch (err) {writeErr(err)}
		}
		
		goMatch(name, hostClient){
			try{
				if(this.lobbyListe.has(''+name)){
					this.lobbyListe.get(''+name).goMatch(hostClient);
				}
			} catch (err) {writeErr(err)}
		}
		
		newPlayerAnzahl(name, hostClient, anzahl){
			try{
				this.lobbyListe.get(''+name).newPlayerAnzahl(hostClient, anzahl);
			} catch (err) {writeErr(err)}
		}
		
		connectNamespaceToLobby(name, socketNamespace, socketIDLobby){
			try{
				this.lobbyListe.get(''+name).connectNamespaceToLobby(socketNamespace, socketIDLobby);
			} catch (err) {writeErr(err)}
		}
		
		socketNamespaceDisconnected(name, socketNamespace){
			try{
				if(this.lobbyListe.has(''+name)){
					this.lobbyListe.get(''+name).socketNamespaceDisconnected(socketNamespace);
				}
			} catch (err) {writeErr(err)}
		}
	}
		
	//################# Lobby Class #####################

	class lobbyClass {
		constructor(name, pass, socketHost){
			this.name = name;
			this.pass = pass;
			this.playerlist = new playerlistClass(socketHost);
			this.openlobby = true;
			this.playeranzahl = 1;
			this.ioNamespace = createNamespace(name, socketHost);
			this.connectToLobby(pass, socketHost);
		}
		
		connectToLobby(pass, socketClient){
			try{
				if(this.pass === pass){
					if(this.IsNochPlatzInPlayerliste()){
						this.playerlist.addPlayer(socketClient);
						socketClient.namespace = this.name;
						io.to(socketClient.id).emit('createNamespaceClient', {name: this.name});
					}else{
						io.to(socketClient.id).emit('lobbyFull');
					}
				}else{
					io.to(socketClient.id).emit('wrongPassword');
				}
			} catch (err) {writeErr(err)}
		}
		
		IsNochPlatzInPlayerliste(){
			try{
				return this.playerlist.getPlayerAnzahl() < this.playeranzahl;
			} catch (err) {writeErr(err)}
		}
		
		goMatch(hostClient){
			try{
				if(hostClient.id === this.playerlist.host.id){
					this.openlobby = false;
					console.log(new Date().toISOString() + ";goMatch;playeranzahl="+this.playeranzahl+";"+hostClient.id);
				}
			} catch (err) {writeErr(err)}
		}
		
		newPlayerAnzahl(hostClient, anzahl){
			try{
				if(hostClient.id === this.playerlist.host.id){
					this.playeranzahl = anzahl;
				}
			} catch (err) {writeErr(err)}
		}
		
		getLobbyInfo(){
			try{
				return [this.name, this.playeranzahl, this.playerlist.getPlayerAnzahl(), this.openlobby];
			} catch (err) {writeErr(err)}
		}
		
		/**
		*	reason: 0=selbst(in der lobby), 1=NamespaceDisconnected (ingame), 2=LobbyClosed von Host
		*/
		leaveLobby(socketClient, reason){ 
			try{
				this.playerlist.leavePlayer(socketClient);
				
				if(this.playerlist.host === socketClient){
					this.playerlist.disconnectAllPlayer();
					return true;
				}else{
					io.to(socketClient.id).emit('lobbyclosed', reason);
					socketClient.disconnect(true);
					io.to(this.playerlist.host.id).emit('clientDisconnectedFromLobby', [socketClient.id]);
					return false;
				}
			} catch (err) {writeErr(err)}
		}
		
		connectNamespaceToLobby(socketNamespace, socketIDLobby){
			try{
				this.playerlist.connectNamespaceToLobby(socketNamespace, socketIDLobby);
			} catch (err) {writeErr(err)}
		}
		
		socketNamespaceDisconnected(socketNamespace){
			try{
				if(socketNamespace.socketLobby !== undefined){
					socketNamespace.socketLobby.disconnect(true);
				}
			} catch (err) {writeErr(err)}
		}
	}

	//################# playerlist Class #####################

	class playerlistClass {
		constructor(sockethost){
			this.host = sockethost;
			this.liste = [];
		}
		
		addPlayer(socketClient){
			try{
				this.liste.push(socketClient);
			} catch (err) {writeErr(err)}
		}
		
		leavePlayer(socketClient){
			try{
				const index = this.getPlayerIndexInList(socketClient);
				if (index > -1) {
					this.liste.splice(index, 1);
					return true;
				}else{
					return false;
				}
			} catch (err) {writeErr(err)}
		}
		
		isPlayerInList(socketClient){
			try{
				return this.liste.contains(socketClient);
			} catch (err) {writeErr(err)}
		}
		
		getPlayerIndexInList(socketClient){
			try{
				return this.liste.indexOf(socketClient);
			} catch (err) {writeErr(err)}
		}
		
		getPlayerAnzahl(){
			try{
				return this.liste.length;
			} catch (err) {writeErr(err)}
		}
		
		connectNamespaceToLobby(socketNamespace, socketIDLobby){
			try{
				for(let socket of this.liste){
					if(socket.id === socketIDLobby){
						socket.socketNamespace = socketNamespace;
						socketNamespace.socketLobby = socket;
						return;
					}
				}
				socketNamespace.disconnect(true);
			} catch (err) {writeErr(err)}
		}
		
		disconnectAllPlayer(){
			try{
				for(let socket of this.liste){
					try{
						socket.socketNamespace.disconnect(true);
						socket.disconnect(true);
					}catch(err){
					}
				}
			} catch (err) {writeErr(err)}
		}
	}


	//###################### HELPER #################

	function isStringAlphanumeric(inputtxt) {
		let letterNumber = /^[0-9a-zA-Z_-]+$/;
		if (inputtxt.match(letterNumber)){
			return true;
		} else {
			return false;
		}
	}
	
	function writeErr(err){
		fs.writeFileSync("ERROR_" + new Date().getTime() + ".txt", 'Message: ' + err.message + '\nStack: ' + err.stack);
	}

