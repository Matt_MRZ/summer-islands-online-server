🏝 Summer Islands - Online Server 🏝

# Overview 

Summer Islands is a HTML5 game with CreateJS as frontend library ([createjs.com](https://www.createjs.com/)) and NodeJS as backend ([nodejs.org](https://nodejs.org/en/)). For compilation as executable we use the perfect tool Web2Executable from jyapayne ([github.com/jyapayne/Web2Executable](https://github.com/jyapayne/Web2Executable)). Basically it is a simple interface to use the NW.js library ([nwjs.io/blog](https://nwjs.io/blog/)).
This is the simple framework of software we use. Since we have published Summer Islands on Steam we need a library to integrate the Steam functions. For example Archievments, Steamcloud and so on. There we use the library Greenworks (github.com/greenheartgames/greenworks)

The online server is an add-on module that allows you to play a match with friends via a server. In contrast to a match via LAN or port activation, you only have to start this module on a server and enter the IP and port in the settings.json in Summer Islands.



**Connections Typs:**

LAN:

![](http://summerislands.de/wp-content/uploads/2021/02/Sketch_Singleplayer-2.jpg)

Port Forwarding:

![](http://summerislands.de/wp-content/uploads/2021/02/Sketch_LanPortForwarding.jpg)

The Online Server (red rectangle):

![](http://summerislands.de/wp-content/uploads/2021/02/Sketch_OnlineServer-2.jpg)

# Details 

**Only Tested on Debian 10.0 / 64 bit!**

1. Connect to your Server for the filetransfer (example: WinSCP).
2. Copy the `Online Server` folder to your server.
3. Extract the `node_modules.zip` inside the `Online Server` folder.
```
|..\Online Server
    |--Host.js
    |--index.html
    |--node_modules
        |--.bin
        |..
```
4. Connect to your Server for the shell interaction (example: PuTTY)
5. Navigate to the Online Server folder
6. Update your Debian, if not already done
```
sudo apt update && sudo apt upgrade
```
7. install curl, if not already done
```
sudo apt install curl
```
8. install NodeJS, if not already done
```
curl -fsSL https://deb.nodesource.com/setup_12.x | bash -

apt-get install -y nodejs
```
9. install forever, for the background tasks ([https://www.npmjs.com/package/forever](https://www.npmjs.com/package/forever))
```
npm install forever -g
```
10. Start the Server as background tast.
```
forever start Host.js
```
11. Check if the server is running. 
Open a browser and enter your Debian Server IP and the Host.js port (default: 21621). 
It should looks like this:

 ![](http://summerislands.de/wp-content/uploads/2021/02/OnlineServer_Lobbys.jpg)

12. Every player who wants to start or join a match over your server has to change the 

GAMECONNECTOR_IP and 

GAMECONNECTOR_PORT (default: 21621) 

in the settings.json in the Summer Islands game.

![](http://summerislands.de/wp-content/uploads/2021/02/settings-1.jpg)

13.  **HAVE FUN! **🏝 

**If you have any problems, please feel free to contact us!**

# Contact 
📩  **Server@SummerIslands.de**

🏝  [http://www.SummerIslands.de](http://summerislands.de/)

🕹  [https://store.steampowered.com/app/731650/Summer_Islands/](https://store.steampowered.com/app/731650/Summer_Islands/)

🐤  [https://twitter.com/SummerIslandsQA](https://twitter.com/SummerIslandsQA)

# License 
© [Creative Commons Attribution-Noncommercial 4.0 License (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/legalcode)
